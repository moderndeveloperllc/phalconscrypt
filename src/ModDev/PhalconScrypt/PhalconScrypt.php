<?php
namespace ModDev\PhalconScrypt;

use Phalcon\Security;

/**
 * Class to implement scrypt hashing in the Phalcon framework. Requires scrypt extension. This provides hashes that are
 * in the same MCF as {@see https://github.com/wg/scrypt} - a Java implementation of scrypt.
 *
 * @author Mark Garrett <mark@moderndeveloperllc.com>
 */
class PhalconScrypt extends Security
{

    /**
     *
     * @var int The key length
     */
    private static $keyLength = 32;

    /**
     *
     * @var string The MCF identifier for this version of the hashing algorithim.
     */
    private static $hashMcfVersion = 's0';

    /**
     * Create an scrypt password hash
     *
     * @param string $password The clear text password
     * @param int    $N        (Optional) The CPU difficultly (must be a power of 2, > 1)
     * @param int    $r        (Optional) The memory difficultly
     * @param int    $p        (Optional) The parallel difficultly
     *
     * @return string The hashed password
     */
    public function scryptHash($password, $N = 16384, $r = 8, $p = 1)
    {
        if ($N == 0 || ($N & ($N - 1)) != 0) {
            throw new \InvalidArgumentException("N must be > 0 and a power of 2");
        }

        if ($N > PHP_INT_MAX / 128 / $r) {
            throw new \InvalidArgumentException("Parameter N is too large");
        }

        if ($r > PHP_INT_MAX / 128 / $p) {
            throw new \InvalidArgumentException("Parameter r is too large");
        }

        $salt = $this->getSaltBytes();
        $hash = scrypt($password, $salt, $N, $r, $p, self::$keyLength);
        $params = dechex(log($N, 2)) . sprintf('%02x', $r) . sprintf('%02x', $p);

        return '$' . self::$hashMcfVersion . '$' . $params . '$' . base64_encode($salt) . '$' . base64_encode($hash);
    }

    /**
     * Check a clear text password against a hash
     *
     * @param string $password The clear text password
     * @param string $hash     The hashed password
     *
     * @return boolean If the clear text matches
     */
    public function scryptCheckHash($password, $hash)
    {
        // Is there actually a hash in the modified MCF format?
        if (!strlen($hash) || substr($hash, 0, 1) !== '$') {
            return false;
        }

        list ($version, $params, $salt, $encodedHash) = explode('$', substr($hash, 1), 5);

        // Do we have a version we can check?
        if ($version !== self::$hashMcfVersion) {
            return false;
        }

        $N = (int) pow(2, hexdec(substr($params, 0, -4)));
        $r = (int) substr($params, -4, 2);
        $p = (int) substr($params, -2, 2);

        // No empty fields?
        if (empty($salt) || empty($hash) || !is_numeric($N) || !is_numeric($r) or !is_numeric($p)) {
            return false;
        }

        $calculated = scrypt($password, base64_decode($salt), $N, $r, $p, self::$keyLength);

        // Use compareStrings to avoid timeing attacks
        return self::compareStrings(base64_decode($encodedHash), $calculated);
    }

    /**
     * Return the hash algorithm for a hashed password. Used primarily to check if the hashed password is bcrypt or
     * scrypt.
     *
     * @param string $hash The hashed password
     *
     * @return string The hash algorithm. Returns 'unknown' if hash type not found.
     */
    public function getHashType($hash)
    {
        $type = '';
        $hashParts = explode('$', $hash);

        switch ($hashParts[1]) {
        	case self::$hashMcfVersion:
        	    $type = 'scrypt';
        	    break;

        	case '2a':
        	case '2x':
        	case '2y':
        	    $type = 'bcrypt';
        	    break;

        	case '1':
        	    $type = 'md5';
        	    break;

        	case '5':
        	    $type = 'sha-256';
        	    break;

    	    case '6':
    	        $type = 'sha-512';
    	        break;

	        case 'sha1':
	            $type = 'sha1';
	            break;

    	    default: $type = 'unknown';
        }

        return $type;
    }

    /**
     * Zend Framework (http://framework.zend.com/)
     *
     * @link      http://github.com/zendframework/zf2 for the canonical source repository
     * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
     * @license   http://framework.zend.com/license/new-bsd New BSD License
     *
     * Compare two strings to avoid timing attacks
     *
     * C function memcmp() internally used by PHP, exits as soon as a difference is found in the two buffers. That makes
     * possible of leaking timing information useful to an attacker attempting to iteratively guess the unknown string
     * (e.g. password).
     *
     * @param string $expected
     * @param string $actual
     *
     * @return boolean If the two strings match.
     */
    public static function compareStrings($expected, $actual)
    {
        $expected    = (string) $expected;
        $actual      = (string) $actual;
        $lenExpected = strlen($expected);
        $lenActual   = strlen($actual);
        $len         = min($lenExpected, $lenActual);

        $result = 0;
        for ($i = 0; $i < $len; $i ++) {
            $result |= ord($expected[$i]) ^ ord($actual[$i]);
        }
        $result |= $lenExpected ^ $lenActual;

        return ($result === 0);
    }
}
