PhalconScrypt
=============


# Abandonware
**Like all old code, this library is consigned to the history books. Use the much better and faster `password_hash()` in PHP with the PASSWORD_ARGON2ID algorithm.**


This class is an extension to the built-in Phalcon\Security class that add functions to generate scrypt hashes that are compatible with the wg/scrypt Java implementation.

-------------

Installing via Composer
=======================
Install composer in a common location or in your project:

```bash
curl -s http://getcomposer.org/installer | php
```

Create the composer.json file as follows:

```json
{
    "require": {
        "moderndeveloperllc/phalconscrypt" : "1.0.*"
    }
}
```

Run the composer installer:

```bash
php composer.phar install
```

---------------

Service Configuration
---------------

You will need to overwrite the existing Security service with the new class. You will have access to all the `Phalcon\Security` functions as the `PhalconScrypt` class extends that class.

```
<?php
use ModDev\PhalconScrypt\PhalconScrypt;

$di->set(
    'security',
    function () {
        return new PhalconScrypt();
    },
    true
);
```

-------------

Loader Configuration
-------------

**Library Installed Manually**

```
<?php
//Make sure to register namespace
$loader->registerNamespaces(array(
    //Other namespaces
  'ModDev\PhalconScrypt' => $path->to->library . '/src/ModDev/PhalconScrypt'
));
```

If you install the library manually, be sure to load the library in your autoloader. The easiest to do is in a `registerNamespaces()` function. There is only one class at the moment, but that could change in the future.

**Library Installed via Composer**
```
<?php
// Your existing loader config...
// ...

require_once __DIR__ . '/../../vendor/autoload.php';
```

Usage
------------

### Hash a password

```
<?php

$hashedPassword = $this->security->scryptHash('myReallyGreatPassword');
```

### Check the hashed password against an user input

```
<?php

$passwordWorksBoolean = $this->security->scryptCheckHash($cleartextPassword, $hashedPassword);
```

### Return the hash algorithm

```
<?php

$passwordhashType = $this->security->getHashType($hashedPassword);
```